## v0.0.1 -- 2018-06-19

* Added mnist Convolutional neural net example.

## v0.0.2 -- 2018-07-17

* Added some minor improvements

## v0.1.0 -- 2018-07-17

* Added new netword: A breast cancer predicter.

## 0.2.0 -- 2018-07-19

* Added a new network (parkinsons): A network to tell if a patient has parkinsons based on their brainwaves.
* Optimized the other networks, namely 'breast_cancer'.
