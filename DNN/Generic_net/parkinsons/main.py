#!/usr/bin/python3

import tflearn
from tflearn.data_utils import load_csv

X, Y = load_csv("data.csv", columns_to_ignore=[0], n_classes=2, target_column=17,
                categorical_labels=True, has_header=True)

momentum = tflearn.Momentum(learning_rate=0.01, lr_decay=0.96)

net = tflearn.input_data(shape=[None, 22])
net = tflearn.fully_connected(net, 512, activation='relu')
net = tflearn.dropout(net, 0.8)
net = tflearn.fully_connected(net, 1024, activation='relu')
net = tflearn.dropout(net, 0.8)
net = tflearn.fully_connected(net, 1024, activation='relu')
net = tflearn.dropout(net, 0.8)
net = tflearn.fully_connected(net, 512, activation='relu')
net = tflearn.fully_connected(net, 2, activation='softmax',)
net = tflearn.regression(net, optimizer='adagrad', loss='categorical_crossentropy')

testX = [

    [95.05600, 120.10300, 91.22600, 0.00532, 0.00006, 0.00268, 0.00332,
     0.00803, 0.02838, 0.25500, 0.01441, 0.01725, 0.02444, 0.04324, 0.01022,
     21.86200, 0.547037, 0.798463, -5.011879, 0.325996, 2.432792, 0.271362],

    [197.07600,206.89600,192.05500,0.00289,0.00001,0.00166,
     0.00168,0.00498,0.01098,0.09700,0.00563,0.00680,0.00802,0.01689,
     0.00339,26.77500,0.422229,0.741367,-7.348300,0.177551,1.743867,0.085569]

]

cOpt = input("What would you like to do (fit/pred): ")
model = tflearn.DNN(net, tensorboard_dir="logs")
if cOpt == "fit":
    model.fit(X, Y, n_epoch=2000, show_metric=True, batch_size=32, shuffle=True,
              run_id="parkinsons")
    model.save("logs/parkinsons.model")
    quit(0)

elif cOpt == "pred":
    model.load('logs/parkinsons.model')

    pred = model.predict(testX)

    print("0: " + str(pred[0][0] * 100)[:5] + " %")
    print("1: " + str(pred[0][1] * 100)[:5] + " %\n")

    print("0: " + str(pred[1][0] * 100)[:5] + " %")
    print("1: " + str(pred[1][1] * 100)[:5] + " %")

    quit(0)

else:
    print("Invalid option!")
    quit(0)