#!/usr/bin/python3

import tflearn
from tflearn.data_utils import load_csv

data, labels = load_csv("bc.csv", target_column=0, categorical_labels=True, columns_to_ignore=[7, 8], n_classes=2)

adam = tflearn.Adam(0.001)
momentum = tflearn.Momentum(0.01, lr_decay=0.96)

net = tflearn.input_data(shape=[None, 7])
net = tflearn.fully_connected(net, n_units=512, activation='relu')
net = tflearn.fully_connected(net, n_units=1024, activation='relu')
net = tflearn.dropout(net, 0.8)
net = tflearn.fully_connected(net, n_units=1024, activation='relu')
net = tflearn.dropout(net, 0.8)
net = tflearn.fully_connected(net, n_units=128, activation='relu')
net = tflearn.fully_connected(net, n_units=2, activation='softmax')
net = tflearn.regression(net, optimizer=adam, loss='categorical_crossentropy')

option = input("Select an operation (fit/pred): ")

# Initialize some testing data.
testX = [

    [40, 70, 20, 0, 0, 2, 0],   # Expected: 0
    [50, 70, 50, 9, 1, 2, 0]    # Expected: 1

]

model = tflearn.DNN(net, tensorboard_dir='logs')
if option == "fit":
    model.fit(data, labels, n_epoch=1000, batch_size=16, show_metric=True, shuffle=True, run_id='b_cancer')
    model.save('logs/bcancer.model')
elif option == "pred":
    model.load("logs/bcancer.model")
    pred = model.predict(testX)

    for p in pred:
        print("0: " + str(p[0] * 100)[:5] + " %")
        print("1: " + str(p[1] * 100)[:5] + " %\n")

else:
    print("No such option!")
    quit(0)
