#!/usr/bin/python3

import numpy as np
import tflearn
from tflearn.data_utils import image_preloader
from tflearn.datasets import mnist

data, labels, testX, testY = mnist.load_data(one_hot=True)

data = data.reshape([-1, 28, 28, 1])
testX = testX.reshape([-1, 28, 28, 1])

momentum = tflearn.Momentum(learning_rate=0.1, lr_decay=0.96)

net = tflearn.input_data(shape=[None, 28, 28, 1])
net = tflearn.conv_2d(net, 5, 6, activation='relu')
net = tflearn.conv_2d(net, 5, 6, activation='relu')
net = tflearn.fully_connected(net, 512, activation='relu')
net = tflearn.dropout(net, 0.85)
net = tflearn.fully_connected(net, 10, activation='softmax')
net = tflearn.regression(net, optimizer=momentum, loss='categorical_crossentropy')

model = tflearn.DNN(net, tensorboard_dir='log')

choice = input("Choose an option (fit/pred): ")

if choice == "fit":
    model.fit(data, labels, validation_set=[testX, testY], n_epoch=5,
              batch_size=64, show_metric=True, shuffle=True, run_id='mnist')
    model.save("mnist.model")

elif choice == "pred":
    model.load("mnist.model")

    pX = image_preloader("images.txt", image_shape=(28, 28), grayscale=True, mode='file')
    arr = np.array(pX[0])
    arr = arr.reshape([-1, 28, 28, 1])

    output = model.predict(arr)

    def getMaxIndex(arr):
        highest = 0
        hIndex = 0
        for i in range(len(arr)):
            if arr[i] > highest:
                highest = arr[i]
                hIndex = i

        return hIndex

    for i in range(len(output)):
        print("Highest index (test #" + str(i) + "): " + str(getMaxIndex(output[i])))

else:
    print("Invalid option!")
